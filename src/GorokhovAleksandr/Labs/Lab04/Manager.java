public class  Manager extends Employee implements Project{
	String project;
	int costProject;
	int percentWork;
	
	public Manager(int id, String name, String project, int costProject, int percentWork) {
		super(id, name);			
		this.project = project;
		this.costProject = costProject;
		this.percentWork = percentWork;
	}
	public double CalcProject(int costProject, int percentWork) {		
		double project = costProject*percentWork/100;
		return project;
	}	
	public void Calc(){
		setPayment(CalcProject(costProject, percentWork));
	}
}

class ProjectManager extends Manager implements Heading{
	int numPeople;
	public ProjectManager(int id, String name, String project, int costProject, int percentWork, int numPeople) {
        super(id, name, project, costProject, percentWork);
		this.numPeople = numPeople;
    }
	public double CalcHeader(int numPeople) {		
		double header = numPeople*10000;
		return header;
	}
	@Override
	public void Calc(){
		setPayment(CalcProject(costProject, percentWork)+CalcHeader(numPeople));
	}
}

final class SeniorManager extends ProjectManager{
	public SeniorManager(int id, String name, String project, int costProject, int percentWork, int numPeople) {
        super(id, name, project, costProject, percentWork, numPeople);
    }	
}