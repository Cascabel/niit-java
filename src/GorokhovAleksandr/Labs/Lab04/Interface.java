interface WorkTime {
	double CalcWorkTime(int Hour, int base);	
}

interface Project {
	double CalcProject(int costProject, int percentWork);	
}

interface Heading {
	double CalcHeader(int numPeople);	
}