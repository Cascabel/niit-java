

public class DekanatDemo {
	public static void main (String[] args) {
		
		Dekanat d1 = new Dekanat();
		//Добавление студентов
		d1.addStudent();
		//Добавление групп
		d1.addGroup();
		//Зачисление студентов в группы
		for (int i=0; i<10; i++) {
			d1.Groups[0].addStudent(d1.Students[i]);
			d1.Students[i].setGroup(d1.Groups[0]);
		}
		for (int i=10; i<20; i++) {
			d1.Groups[1].addStudent(d1.Students[i]);
			d1.Students[i].setGroup(d1.Groups[1]);
		}	
		for (int i=20; i<30; i++) {
			d1.Groups[2].addStudent(d1.Students[i]);
			d1.Students[i].setGroup(d1.Groups[2]);
		}
		//Выбор старост в группах
		d1.setHeadGroup();
		//Выставление оценок
		d1.addRandomMarks();
		//Перевод студента из группы в группу
		d1.moveStudent(d1.Students[3],d1.Groups[1]);
		//Отчисление студента
		d1.removeStudent(d1.Students[23]);
		//Перевод студента из группы в группу
		d1.moveStudent(d1.Students[25],d1.Groups[0]);
		//сохранение данных в файл		
		d1.RefreshFile();
		//вывод результатов
		d1.PrintStudent();
				
	}
}