import java.io.*;
import java.net.*;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;


public class ClientJavaFX extends Application {
       public static void main(String[] args) {
               Application.launch(args);
}
       @Override
       public void start(Stage stage) {
		   
		   try {
			Client4.ConnectToServer();
           }
			catch (IOException e1) {
			System.err.println("������ ������/������ �����");
			}
        // Create buttons
        Button TimeBtn = new Button("Time");
		Button AphorismBtn = new Button("Aphorism");
		Button RateBtn = new Button("Rate (USD/RUB)");
        Button ExitBtn = new Button("Exit");
		
		Label TimeLbl = new Label();
		Label AphorismLbl = new Label();
		Label RateLbl = new Label();
		Label ExitLbl = new Label();
		
        // Add the event handler for the Say Hello button
        TimeBtn.setOnAction(e -> {
			String UpdateTime;
			try {
				UpdateTime = Client4.UpdateToServer("DATE");
				TimeLbl.setText(UpdateTime);
			}
			catch (IOException e1) {
				System.err.println("������ ������/������ �����");
			}
		    
            /*String name = nameFld.getText();
            if (name.trim().length() > 0) {
             msg.setText("Hello " + name);
            } else {
             msg.setText("Hello there");
            }
            msg.setStyle(styleFld.getText());*/
        });
        // Add the event handler for the Exit button
       AphorismBtn.setOnAction(e -> {
		   String UpdateAphorism;
			try {
				UpdateAphorism = Client4.UpdateToServer("APHORISM");
				AphorismLbl.setText(UpdateAphorism);				
			}
			catch (IOException e1) {
				System.err.println("������ ������/������ �����");
			}
		    
		});
		RateBtn.setOnAction(e -> {
			String UpdateRate;
			try {
				UpdateRate = Client4.UpdateToServer("RATE");
				RateLbl.setText(UpdateRate);								
			}
			catch (IOException e1) {
				System.err.println("������ ������/������ �����");
			}		    
		});
		ExitBtn.setOnAction(e -> Platform.exit());
        // Create the root node
        HBox root = new HBox();
       // Set the vertical spacing between children to 5px
       root.setSpacing(30);
       // Add children to the root node

       VBox buttons = new VBox();
	   VBox lbls = new VBox(10);
       buttons.getChildren().addAll(TimeBtn, AphorismBtn, RateBtn, ExitBtn);
	   lbls.getChildren().addAll(TimeLbl, AphorismLbl, RateLbl);
       root.getChildren().addAll(buttons, lbls);
       Scene scene = new Scene(root, 1100, 250);
	   	   
       stage.setScene(scene);
	   stage.setResizable(false);
       stage.setTitle("Net Application");
       stage.show();
   }
}




class Client4 {
	//static BufferedReader in;
	//static PrintWriter out;
	static Socket server;
  public static void ConnectToServer() throws IOException {
    System.out.println("������ ���������");
    //Socket server = null;

    /*if (args.length==0) {
      System.out.println("�������������: java Client hostname");
      System.exit(-1);
    }*/

    System.out.println("����������� � �������� "+"127.0.0.1");

    server = new Socket("127.0.0.1",1234);
    
    //BufferedReader inu = 
    //   new BufferedReader(new InputStreamReader(System.in));
    
    /*out.close();
    in.close();
    inu.close();
    //server.close();*/
  }
  
  public static String UpdateToServer(String fuser) throws IOException {
	  BufferedReader in  = new BufferedReader(new  InputStreamReader(server.getInputStream()));
	  PrintWriter out = new PrintWriter(server.getOutputStream(),true);
	  String fserver;		 
      out.println(fuser);
      fserver = in.readLine();
      System.out.println(fserver);
      return fserver;
  }
  
}