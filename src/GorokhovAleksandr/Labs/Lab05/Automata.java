enum STATES	{
		ON(1), 
		OFF(2), 
		WAIT(3), 
		ACCEPT(4),
		CHECK(5),
		COOK(6);
		final int number;
		STATES(int number)
		{
		   this.number=number;
		}

		public int get() 
		{
		   return number;
		}
	}	


class Automata {
		
	int cash = 0;
	String[] menu = {"1. Cappuccino","2. Espresso", "3. Americano", "4. Latte", "5. Mocha", "6. Hot chocolate", "7. Double chocolate", "8. Double espresso", "9. Ristretto", "10. Double cappuccino"};
	int[] prices = {30,25,20,25,25,30,35,30,30,35};
	int drink;
	STATES state = STATES.OFF;	
	
	
	
	void on() {state = STATES.WAIT;}		
		
	void off() {state = STATES.OFF;}
	
	void coin(int nextcoin) {					
		cash += nextcoin;
		state = STATES.ACCEPT;
	}
	
	int getCash() {return cash;}
	
	void printMenu() {
		System.out.println ("\n�������:");
		for (int i = 0; i<menu.length; i++ ) {	
			System.out.println(menu[i] + " - " + prices[i] + " ������");
		}
		System.out.println();
	}
	STATES getState() {return state;}
		
	void choice(int n) {drink = n;}
	
	boolean check() {
		state = STATES.CHECK;
		if (cash>=prices[drink]) {			
			return true;}
		else {
			state = STATES.WAIT;
			return false;}
			
	}
	
	int money() {return cash-prices[drink];}
	
	void cancel() {state = STATES.OFF;}
	void cook() {
		/*try{
            Thread.sleep(2000);
            }
        catch(InterruptedException t){
        }*/
		state = STATES.COOK;			
		}
	void finish() {state = STATES.WAIT;}
}
