import java.io.*;
import java.net.*;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import javafx.stage.Stage;
import javafx.scene.layout.*;
import javafx.geometry.*;
import javafx.scene.image.*;


public class AutoCoffee2 extends Application {
	
	Automata Demo1;
	Label DispLbl;
	Label CashLbl;
	Label TrifleLbl;
	
	
	public static void main(String[] args) {
               Application.launch(args);
}
       	   
	   @Override	   
       public void start(Stage stage) {
		   
		   Demo1 = new Automata();
		   		  
        // Create buttons
        Button Btn1 = new Button("Cappuccino\n30 ������");	
		Btn1.setMinWidth(140);			
		Button Btn2 = new Button("Espresso\n25 ������");
		Btn2.setMinWidth(140);
		Button Btn3 = new Button("Americano\n20 ������");
		Btn3.setMinWidth(140);
		Button Btn4 = new Button("Latte\n25 ������");
		Btn4.setMinWidth(140);
		Button Btn5 = new Button("Mocha\n25 ������");
		Btn5.setMinWidth(140);
		Button Btn6 = new Button("Hot chocolate\n30 ������");
		Btn6.setMinWidth(140);
		Button Btn7 = new Button("Double chocolate\n35 ������");
		Btn7.setMinWidth(140);
		Button Btn8 = new Button("Double espresso\n30 ������");
		Btn8.setMinWidth(140);
		Button Btn9 = new Button("Ristretto\n30 ������");
		Btn9.setMinWidth(140);
		Button Btn10 = new Button("Double cappuccino\n35 ������");
		Btn10.setMinWidth(140);
		Button Cash5Btn = new Button("5 ������");
		Button Cash10Btn = new Button("10 ������");
        Button ExitBtn = new Button("Exit");
		
		DispLbl = new Label("����������, ������� ������\n � �������� �������!");
		TrifleLbl = new Label();
		CashLbl = new Label();
				
		Image Image1 = new Image("Image01.jpg");
		ImageView img1 = new ImageView(Image1);
		img1.setFitWidth(300);
        img1.setPreserveRatio(true);
        img1.setSmooth(true);
        img1.setCache(true);
		
		Image Image2 = new Image("Image03.jpg");
		ImageView img2 = new ImageView(Image2);
		img2.setFitWidth(150);
        img2.setPreserveRatio(true);
        img2.setSmooth(true);
        img2.setCache(true);
				
		GridPane root = new GridPane();
				
		
		root.add(img1, 0, 1);	
		root.add(Btn1, 0, 2);		
		root.add(Btn2, 0, 3);		
		root.add(Btn3, 0, 4);
		root.add(Btn4, 0, 5);
		root.add(Btn5, 0, 6);
		root.add(Btn6, 1, 2);
		root.add(Btn7, 1, 3);
		root.add(Btn8, 1, 4);
		root.add(Btn9, 1, 5);
		root.add(Btn10, 1, 6);
		root.add(img2, 0, 7);
		root.add(DispLbl, 2, 1);
		root.add(CashLbl, 2, 2);
		
		root.setHalignment(CashLbl, HPos.CENTER);
		root.setHalignment(DispLbl, HPos.CENTER);
		root.setHalignment(TrifleLbl, HPos.CENTER);
		root.add(Cash5Btn, 2, 3);
		root.setHalignment(Cash5Btn, HPos.CENTER);
		root.add(Cash10Btn, 2, 4);
		root.setHalignment(Cash10Btn, HPos.CENTER);
		root.add(TrifleLbl, 2, 5);
		root.setColumnSpan(img1, 2);
		root.setColumnSpan(img2, 2);
		root.setHalignment(img2, HPos.CENTER);
		
		//root.setGridLinesVisible(true);
		root.setAlignment(Pos.CENTER);
		root.setHgap(10);
		root.setVgap(10);
				
		ColumnConstraints columnConstraints0 = new ColumnConstraints();
		columnConstraints0.setPercentWidth(30);
		ColumnConstraints columnConstraints1 = new ColumnConstraints();
		columnConstraints1.setPercentWidth(30);
		ColumnConstraints columnConstraints2 = new ColumnConstraints();
		columnConstraints2.setPercentWidth(40);
		root.getColumnConstraints().addAll(columnConstraints0, columnConstraints1, columnConstraints2);
		
		Scene scene = new Scene(root, 500, 650);		
		
		stage.setScene(scene);
	    stage.setResizable(false);
        stage.setTitle("Coffee");
        stage.show();
		
		Cash5Btn.setOnAction(e -> {
			Demo1.coin(5);
			String str = Integer.toString(Demo1.getCash());
			CashLbl.setText("�������: " + str + "������");			  
        });
		
		Cash10Btn.setOnAction(e -> {
			Demo1.coin(10);
			String str = Integer.toString(Demo1.getCash());
			CashLbl.setText("�������: " + str + "������");
		});
		
		Btn1.setOnAction(e -> {
			WorkAutoCoffee(0);			
		});		
		Btn2.setOnAction(e -> {
			WorkAutoCoffee(1);
		});
		Btn3.setOnAction(e -> {
			WorkAutoCoffee(2);
		});	
		Btn4.setOnAction(e -> {
			WorkAutoCoffee(3);
		});	
		Btn5.setOnAction(e -> {
			WorkAutoCoffee(4);
		});	
		Btn6.setOnAction(e -> {
			WorkAutoCoffee(5);
		});	
		Btn7.setOnAction(e -> {
			WorkAutoCoffee(6);
		});	
		Btn8.setOnAction(e -> {
			WorkAutoCoffee(7);
		});	
		Btn9.setOnAction(e -> {
			WorkAutoCoffee(8);
		});	
		Btn10.setOnAction(e -> {
			WorkAutoCoffee(9);
		});	
		
		Demo1.on();			
        }
				
		void WorkAutoCoffee(int choice) {
			Demo1.choice(choice);
			if (Demo1.check()==true) {			
				DispLbl.setText("�� ������� " + Demo1.menu[choice].substring(3) + "\n������! ��������� ��������!");
				TrifleLbl.setText("�����: " + Integer.toString(Demo1.money()) + " ������");
				Demo1.cash = 0;
				CashLbl.setText("�������: " + Integer.toString(Demo1.getCash()) + " ������");				
				Demo1.finish();
			}
			else { 
				DispLbl.setText("������������ �������!");
			}	
			Demo1.cook();
		}
	
}